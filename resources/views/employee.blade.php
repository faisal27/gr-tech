@extends('layouts.app')
@section('title', $title)
@section('content')
  <div class="container-fluid mt-3">
    <div class="row">
      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-3">
            <button type="button" onclick="addData()" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah
            </button>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 mt-3">
            <table width="100%" class="table table-striped table-bordered" id="employee_table">
              <thead>
              <tr class="text-center">
                <th width="30">No</th>
                <th>Full Name</th>
                <th>Perusahaan</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="employee_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form action="" id="employee_form" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id" id="id">
            <div class="form-group">
              <label for="first_name" class="form-label">Nama Depan<span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Masukan nama depan">
              <span class="text-danger">
                <strong id="first_name-error"></strong>
              </span>
            </div>
            <div class="form-group">
              <label for="last_name" class="form-label">Nama Belakang<span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Masukan nama belakang">
              <span class="text-danger">
                <strong id="last_name-error"></strong>
              </span>
            </div>
            <div class="form-group">
              <label for="email" class="form-label">Email<span class="text-danger">*</span></label>
              <input type="email" class="form-control" name="email" id="email" placeholder="Masukan email">
              <span class="text-danger">
                <strong id="email-error"></strong>
              </span>
            </div>
            <div class="form-group">
              <label for="phone" class="form-label">Nomor Handphone<span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="phone" id="phone" placeholder="Masukan phone">
              <span class="text-danger">
                <strong id="phone-error"></strong>
              </span>
            </div>
            <div class="form-group">
              <label for="company_id" class="form-label">Perusahaan<span class="text-danger">*</span></label>
              <select name="company_id" id="company_id" class="form-control">
                <option value="">-- Pilih Perusahaan --</option>
                @forelse($companies as $company)
                  <option value="{{ $company->id }}">{{ $company->name }}</option>
                @empty
                  <option value="" disabled>Data belum tersedia</option>
                @endforelse
              </select>
              <span class="text-danger">
                <strong id="company_id-error"></strong>
              </span>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary btn-submit" onclick="submitForm()">Submit</button>
        </div>
      </div>
    </div>
  </div>
@stop
@push('scripts')
  <script>
    let url, type, table;

    const addData = function () {
      resetForm();
      $('#employee_modal').modal('show');
      url = '{{ route('employee.store') }}';
      type = 'post';
    }

    table = $('#employee_table').DataTable({
      processing: true,
      serverSide: true,
      responsive: true,
      destroy: true,
      order: [],

      ajax: {
        "url": '{{ route('employee.datatable') }}',
        "type": "POST",
        "headers": {
          "X-CSRF-TOKEN": "{{ csrf_token() }}",
        },
      },

      columns: [
        {data: 'DT_RowIndex'},
        {data: 'name'},
        {data: 'company', sClass: 'text-center'},
        {data: 'email'},
        {data: 'phone'},
        {data: 'action', sClass: 'text-center'},
      ],
    });

    const submitForm = function () {
      $.ajax({
        headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
        url: url,
        type: type,
        data: $('#employee_form').serialize(),
        dataType: "json",
        beforeSend: function () {
          $(".btn-submit").html('<i class="fa fa-spinner fa-spin"></i> Processing...');
          $(".btn-submit").attr('disabled', true);
        },
        success: function (response) {
          notification(response.status, response.message);
          $(".btn-submit").html('Submit');
          $(".btn-submit").attr('disabled', false);

          if (response.status === 'success') {
            $("#employee_modal").modal('hide');
            resetForm();
            table.ajax.reload();
          }
        },
        error: function (resp) {
          $(".btn-submit").html('Submit');
          $(".btn-submit").attr('disabled', false);
          if (_.has(resp.responseJSON, 'errors')) {
            _.map(resp.responseJSON.errors, function (val, key) {
              $('#' + key + '-error').html(val[0]).fadeIn(1000).fadeOut(5000);
            })
          } else {
            Swal.fire({
              title: "Perhatian!",
              text: resp.responseJSON.message,
              icon: "error"
            });
          }
        }
      });
    }

    const editEmployee = function (id) {
      resetForm();
      $('.modal-title').text('Edit Data');
      $('#employee_modal').modal('show');
      url = '{{ route('employee.update') }}';
      type = 'post';

      $.ajax({
        headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
        url: "{{ route('employee.edit') }}",
        type: "get",
        data: {id: id},
        dataType: "json",
        success: function (response) {
          const data = response.data;
          $('#id').val(data.id);
          $('#name').val(data.name);
          $('#email').val(data.email);
          $('#website').val(data.website);
          $('#phone').val(data.phone);
          $('#company_id').val(data.company_id);
        },
        error: function (xhr, status, error) {
          alert(status + " : " + error);
        }
      });
    }

    const deleteEmployee = function (id) {
      Swal.fire({
        title: "Apa anda yakin?",
        text: "Data akan dihapus dan tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus!",

      }).then((result) => {
        if (result.value) {
          $.ajax({
            headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
            url: "{{ route('employee.delete') }}",
            type: "delete",
            data: {id: id},
            dataType: "json",
            success: function (response) {
              notification(response.status, response.message);

              if (response.status === 'success') {
                table.ajax.reload();
              }
            },
            error: function (xhr, status, error) {
              alert(status + " : " + error);
            }
          });
        } else {
          Swal.fire({
            icon: 'warning',
            title: 'Warning',
            text: 'Dibatalkan',
          });
        }
      });
    }

    const resetForm = function () {
      $('#first_name').val('');
      $('#last_name').val('');
      $('#email').val('');
      $('#phone').val('');
      $('#company_id').val('');
    }
  </script>
@endpush
