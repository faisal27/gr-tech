@extends('layouts.app')
@section('title', $title)
@section('content')
  <div class="container-fluid mt-3">
    <div class="row">
      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-3">
            <button type="button" onclick="addData()" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah
            </button>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 mt-3">
            <table width="100%" class="table table-striped table-bordered" id="company_table">
              <thead>
              <tr class="text-center">
                <th width="30">No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Website</th>
                <th>Logo</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="company_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form action="" id="company_form" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id" id="id">
            <div class="form-group">
              <label for="name" class="form-label">Nama<span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="name" id="name" placeholder="Masukan nama">
              <span class="text-danger">
                <strong id="name-error"></strong>
              </span>
            </div>
            <div class="form-group">
              <label for="email" class="form-label">Email<span class="text-danger">*</span></label>
              <input type="email" class="form-control" name="email" id="email" placeholder="Masukan email">
              <span class="text-danger">
                <strong id="email-error"></strong>
              </span>
            </div>
            <div class="form-group">
              <label for="website" class="form-label">Website<span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="website" id="website" placeholder="Masukan website">
              <span class="text-danger">
                <strong id="website-error"></strong>
              </span>
            </div>
            <div class="form-group">
              <label for="logo" class="form-label">Logo</label>
              <input class="form-control" type="file" name="logo" id="logo">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary btn-submit" onclick="submitForm()">Submit</button>
        </div>
      </div>
    </div>
  </div>
@stop
@push('scripts')
  <script>
    let url, type, table;

    const addData = function () {
      resetForm();
      $('#company_modal').modal('show');
      url = '{{ route('company.store') }}';
      type = 'post';
    }

    table = $('#company_table').DataTable({
      processing: true,
      serverSide: true,
      responsive: true,
      destroy: true,
      order: [],

      ajax: {
        "url": '{{ route('company.datatable') }}',
        "type": "POST",
        "headers": {
          "X-CSRF-TOKEN": "{{ csrf_token() }}",
        },
      },

      columns: [
        {data: 'DT_RowIndex'},
        {data: 'name'},
        {data: 'email'},
        {data: 'website'},
        {data: 'logo', sClass: 'text-center'},
        {data: 'action', sClass: 'text-center'},
      ],
    });

    const submitForm = function () {
      const form = $('#company_form')[0];
      const formData = new FormData(form);
      $.ajax({
        headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
        url: url,
        type: type,
        data: formData,
        dataType: "json",
        processData: false,
        contentType: false,
        beforeSend: function () {
          $(".btn-submit").html('<i class="fa fa-spinner fa-spin"></i> Processing...');
          $(".btn-submit").attr('disabled', true);
        },
        success: function (response) {
          notification(response.status, response.message);
          $(".btn-submit").html('Submit');
          $(".btn-submit").attr('disabled', false);

          if (response.status === 'success') {
            $("#company_modal").modal('hide');
            resetForm();
            table.ajax.reload();
          }
        },
        error: function (resp) {
          $(".btn-submit").html('Submit');
          $(".btn-submit").attr('disabled', false);
          if (_.has(resp.responseJSON, 'errors')) {
            _.map(resp.responseJSON.errors, function (val, key) {
              $('#' + key + '-error').html(val[0]).fadeIn(1000).fadeOut(5000);
            })
          } else {
            Swal.fire({
              title: "Perhatian!",
              text: resp.responseJSON.message,
              icon: "error"
            });
          }
        }
      });
    }

    const editCompany = function (id) {
      resetForm();
      $('.modal-title').text('Edit Data');
      $('#company_modal').modal('show');
      url = '{{ route('company.update') }}';
      type = 'post';

      $.ajax({
        headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
        url: "{{ route('company.edit') }}",
        type: "get",
        data: {id: id},
        dataType: "json",
        success: function (response) {
          const data = response.data;
          $('#id').val(data.id);
          $('#name').val(data.name);
          $('#email').val(data.email);
          $('#website').val(data.website);
        },
        error: function (xhr, status, error) {
          alert(status + " : " + error);
        }
      });
    }

    const deleteCompany = function (id) {
      Swal.fire({
        title: "Apa anda yakin?",
        text: "Data akan dihapus dan tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus!",

      }).then((result) => {
        if (result.value) {
          $.ajax({
            headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
            url: "{{ route('company.delete') }}",
            type: "delete",
            data: {id: id},
            dataType: "json",
            success: function (response) {
              notification(response.status, response.message);

              if (response.status === 'success') {
                table.ajax.reload();
              }
            },
            error: function (xhr, status, error) {
              alert(status + " : " + error);
            }
          });
        } else {
          Swal.fire({
            icon: 'warning',
            title: 'Warning',
            text: 'Dibatalkan',
          });
        }
      });
    }

    const resetForm = function () {
      $('#name').val('');
      $('#email').val('');
      $('#website').val('');
      $('#logo').val('');
    }
  </script>
@endpush
