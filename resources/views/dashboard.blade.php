@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
    <div class="container-fluid mt-3">
        <div class="alert alert-primary" role="alert">
            Selamat datang, {{ \Illuminate\Support\Facades\Auth::user()->name }}
        </div>
    </div>
@stop
