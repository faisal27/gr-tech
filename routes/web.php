<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthenticatedSessionController::class, 'create']);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])
  ->name('dashboard');

Route::group(['middleware' => ['auth', 'check.user']], function () {
  Route::group(['prefix' => 'company'], function() {
    Route::get('/', [CompanyController::class, 'index'])->name('company');
    Route::post('/datatable', [CompanyController::class, 'datatable'])->name('company.datatable');
    Route::get('/edit', [CompanyController::class, 'edit'])->name('company.edit');
    Route::post('/store', [CompanyController::class, 'store'])->name('company.store');
    Route::post('/update', [CompanyController::class, 'update'])->name('company.update');
    Route::delete('/delete', [CompanyController::class, 'destroy'])->name('company.delete');
  });

  Route::group(['prefix' => 'employee'], function() {
    Route::get('/', [EmployeeController::class, 'index'])->name('employee');
    Route::post('/datatable', [EmployeeController::class, 'datatable'])->name('employee.datatable');
    Route::get('/edit', [EmployeeController::class, 'edit'])->name('employee.edit');
    Route::post('/store', [EmployeeController::class, 'store'])->name('employee.store');
    Route::post('/update', [EmployeeController::class, 'update'])->name('employee.update');
    Route::delete('/delete', [EmployeeController::class, 'destroy'])->name('employee.delete');
  });
});

require __DIR__.'/auth.php';
