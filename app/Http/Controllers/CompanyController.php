<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyRequest;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Yajra\DataTables\DataTables;

class CompanyController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|Response
   */
  public function index()
  {
    $title = 'Company';
    return view('company', compact('title'));
  }

  /**
   * datatable for showing company data.
   *
   */
  public function datatable()
  {
    $companies = Company::orderBy('id', 'desc')->get();
    return DataTables::of($companies)
      ->addIndexColumn()
      ->addColumn('website', function ($query) {
        return '<a href="'. $query->website .'" target="_blank">'. $query->website .'</a>';
      })
      ->addColumn('logo', function ($query) {
        if (is_null($query->logo)) {
          return '<span class="text-danger"><strong>-</strong></span>';
        } else {
          return '<img src="'. asset('storage/' . $query->logo) .'" width="80" height="80"/>';
        }
      })
      ->addColumn('action', function ($query) {
        return '<a href="#" class="btn btn-success btn-sm" onclick="editCompany('. $query->id .')"><i class="fa fa-pencil"></i></a> |
                <a href="#" class="btn btn-danger btn-sm" onclick="deleteCompany('. $query->id .')"><i class="fa fa-trash"></i></a>';
      })
      ->rawColumns(['action', 'logo', 'website'])
      ->make(true);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param CompanyRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function store(CompanyRequest $request): \Illuminate\Http\JsonResponse
  {
    $data = $request->all();
    $logo = $request->file('logo');

    if ($request->hasFile('logo')) {
      $data['logo'] = $logo->store('logo', 'public');
    }

    $checkEmail = Company::where('email', $data['email'])->first();
    if (!is_null($checkEmail)) {
      return response()->json(['status' => 'failed', 'message' => 'Email sudah digunakan'], 500);
    }

    $insert = Company::create($data);

    if ($insert) {
      return response()->json(['status' => 'success', 'message' => 'Data berhasil ditambahkan']);
    } else {
      return response()->json(['status' => 'failed', 'message' => 'Data gagal ditambahkan'], 400);
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function edit(Request $request)
  {
    $id = $request->id;
    $data = Company::where('id', $id)->first();

    if (!is_null($data)) {
      return response()->json(['status' => 'success', 'message' => 'Data ditemukan', 'data' => $data]);
    } else {
      return response()->json(['status' => 'failed', 'message' => 'Data tidak ditemukan'], 404);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param CompanyRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function update(CompanyRequest $request): \Illuminate\Http\JsonResponse
  {
    $data = $request->all();
    $logo = $request->file('logo');

    if ($request->hasFile('logo')) {
      $data['logo'] = $logo->store('logo', 'public');
    }

    $company = Company::where('id', $data['id'])->first();
    $checkEmail = Company::where('email', $data['email'])->first();

    if ($company->email != $data['email']) {
      if (!is_null($checkEmail)) {
        return response()->json(['status' => 'failed', 'message' => 'Email sudah digunakan'], 500);
      }
    }

    $update = $company->update($data);
    if ($update) {
      return response()->json(['status' => 'success', 'message' => 'Data berhasil diubah']);
    } else {
      return response()->json(['status' => 'failed', 'message' => 'Data gagal diubah'], 400);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy(Request $request): \Illuminate\Http\JsonResponse
  {
    $id = $request->id;
    $update = Company::where('id', $id)->delete();

    if ($update) {
      return response()->json(['status' => 'success', 'message' => 'Data berhasil dihapus']);
    } else {
      return response()->json(['status' => 'failed', 'message' => 'Data gagal dihapus'], 400);
    }
  }
}
