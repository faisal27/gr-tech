<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Yajra\DataTables\DataTables;
use function PHPUnit\Framework\isEmpty;

class EmployeeController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|Response
   */
  public function index()
  {
    $title = 'Employee';
    $companies = Company::all();
    return view('employee', compact('title', 'companies'));
  }

  /**
   * datatable for showing employee data.
   *
   */
  public function datatable()
  {
    $employees = Employee::with('company')
      ->orderBy('id', 'desc')
      ->get();

    return DataTables::of($employees)
      ->addIndexColumn()
      ->addColumn('company', function ($query) {
        if (empty($query->company)) {
          return '<span class="text-danger"><strong>-</strong></span>';
        } else {
          return $query->company->name;
        }
      })
      ->addColumn('name', function ($query) {
        return $query->full_name;
      })
      ->addColumn('action', function ($query) {
        return '<a href="#" class="btn btn-success btn-sm" onclick="editEmployee('. $query->id .')"><i class="fa fa-pencil"></i></a> |
                <a href="#" class="btn btn-danger btn-sm" onclick="deleteEmployee('. $query->id .')"><i class="fa fa-trash"></i></a>';
      })
      ->rawColumns(['action', 'name', 'company'])
      ->make(true);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param EmployeeRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function store(EmployeeRequest $request): \Illuminate\Http\JsonResponse
  {
    $data = $request->all();

    $checkPhone = Employee::where('phone', $data['phone'])->first();
    if (!is_null($checkPhone)) {
      return response()->json(['status' => 'failed', 'message' => 'No Handphone sudah digunakan'], 500);
    }

    $checkEmail = Employee::where('email', $data['email'])->first();
    if (!is_null($checkEmail)) {
      return response()->json(['status' => 'failed', 'message' => 'Email sudah digunakan'], 500);
    }

    $insert = Employee::create($data);

    if ($insert) {
      return response()->json(['status' => 'success', 'message' => 'Data berhasil ditambahkan']);
    } else {
      return response()->json(['status' => 'failed', 'message' => 'Data gagal ditambahkan'], 400);
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function edit(Request $request)
  {
    $id = $request->id;
    $data = Employee::where('id', $id)->first();

    if (!is_null($data)) {
      return response()->json(['status' => 'success', 'message' => 'Data ditemukan', 'data' => $data]);
    } else {
      return response()->json(['status' => 'failed', 'message' => 'Data tidak ditemukan'], 404);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param EmployeeRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function update(EmployeeRequest $request): \Illuminate\Http\JsonResponse
  {
    $data = $request->all();
    $employee = Employee::where('id', $data['id'])->first();
    $checkEmail = Employee::where('email', $data['email'])->first();
    $checkPhone = Employee::where('phone', $data['phone'])->first();

    if ($employee->email != $data['email'] || $employee->phone != $data['phone']) {
      if (!is_null($checkEmail)) {
        return response()->json(['status' => 'failed', 'message' => 'Email sudah digunakan'], 500);
      }

      if (!is_null($checkPhone)) {
        return response()->json(['status' => 'failed', 'message' => 'No Handphone sudah digunakan'], 500);
      }
    }

    $update = $employee->update($data);
    if ($update) {
      return response()->json(['status' => 'success', 'message' => 'Data berhasil diubah']);
    } else {
      return response()->json(['status' => 'failed', 'message' => 'Data gagal diubah'], 400);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy(Request $request): \Illuminate\Http\JsonResponse
  {
    $id = $request->id;
    $update = Employee::where('id', $id)->delete();

    if ($update) {
      return response()->json(['status' => 'success', 'message' => 'Data berhasil dihapus']);
    } else {
      return response()->json(['status' => 'failed', 'message' => 'Data gagal dihapus'], 400);
    }
  }
}
