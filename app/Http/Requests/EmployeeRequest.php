<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'first_name' => 'required|regex:/^[a-zA-Z ]*$/',
      'last_name' => 'required|regex:/^[a-zA-Z ]*$/',
      'email' => 'required|email',
      'phone' => 'required|regex:/^[0-9]*$/',
      'company_id' => 'required'
    ];
  }
}
