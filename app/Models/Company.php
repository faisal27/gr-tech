<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
  use HasFactory, SoftDeletes;

  protected $table = 'companies';
  protected $fillable = ['name', 'email', 'website', 'logo'];

  public function employee()
  {
    return $this->hasMany(Employee::class, 'company_id');
  }
}
